import Postmate from "./postmate.js";
const handshake = new Postmate.Model({
    // Expose your model to the Parent. Property values may be functions, promises, or regular values
    height: () => document.height || document.body.offsetHeight,
    width: () => "100%",
  });
   
  // When parent <-> child handshake is complete, events may be emitted to the parent
  handshake.then(parent => {
    parent.emit('some-event', 'Hello, World!');
    document.getElementById("default-value-from-parent").innerHTML = parent.model.foo; // "bar"
    document.getElementById("test-value-from-parent").innerHTML = parent.model.testvalue; // This is a test value that is set in the parent.html
  });
  