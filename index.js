import  Postmate from "./postmate.js";
Postmate.debug = true;
const handshake = new Postmate({
  container: document.getElementById("test"), // Element to inject frame into
  url: "http://localhost:3000/iframe.html", // Page to load, must have postmate.js. This will also be the origin used for communication.
  name: "custom-iframe-name", // Set Iframe name attribute. Useful to get `window.name` in the child.
  classListArray: ["customClass","customClass2"], //Classes to add to the iframe via classList, useful for styling.
  model: { foo: 'bar', testvalue : "testvalue" }
});
handshake.then((child) => {
  // Fetch the height property in child.html and set it to the iFrames height
  child
    .get("height")
    .then((height) => (child.frame.style.height = `${height}px`));

    child
    .get("width")
    .then((width) => (child.frame.style.width = `${width}`));


  // Listen to a particular event from the child
  child.on("some-event", (data) => console.log(data)); // Logs "Hello, World!"
});
// handshake.then(child => {
//     child.call('sayHi', 'Hello, World!');
//   });

//click event button
const destroyIframe = document.getElementById("destroy-iframe");
destroyIframe.addEventListener("click", () => {
    handshake.then(child => child.destroy());
})

